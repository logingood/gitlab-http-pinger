require 'net/http'

@config = {
  STOP_IN_SECONDS: 360,
  URL_TO_PING: 'http://gitlab.com',
  FOLLOW_REDIRECT: 'true',
  LIMIT_REDIRECT: 3
}

@config.each do |k,v|
  @config[k] = ENV.fetch(k.to_s, v)
end

@failed = "failed:"
@success = "success:"
@warn = "warning:"

def time_diff(start, finish)
  (finish - start) * 1000.0
end

def get_http_page(uri, limit_redirect)
  begin
    response = Net::HTTP.get_response(uri)
  rescue => e
    puts "#{failed} error occured while pinging #{url}: #{e}"
  end

  case response
  when Net::HTTPMovedPermanently,
       Net::HTTPFound,
       Net::HTTPSeeOther,
       Net::HTTPTemporaryRedirect then
    if limit_redirect < 0
      puts "#{@failed} error we reached redirect limit #{limit_redirect}"
    end
    puts "#{@warn} redirected to #{response['location']} http code #{response.code}"
    limit_redirect=-1
    get_http_page(URI(response['location']), limit_redirect)
  else
    response
  end
end

def get_avg(list)
  list.inject{ |sum, el| sum + el }.to_f / list.size
end

puts @config

start_time = Time.now.to_i
url = URI(@config[:URL_TO_PING])
limit = @config[:LIMIT_REDIRECT].to_i
avg_diff = []

loop do
  start = Time.now
  response = get_http_page(url, limit)
  stop = Time.now
  msec_diff = time_diff(start, stop)
  avg_diff << msec_diff

  if response
    puts "#{@success} pinged #{@config[:URL_TO_PING]}: got response code #{response.code} and response time #{msec_diff} msec"
  else
    puts "#{@failed} failed: didn't get response for #{@config[:URL_TO_PING]}"
  end

  if Time.now.to_i - start_time >= @config[:STOP_IN_SECONDS].to_i
    puts "\n\n#{@config[:STOP_IN_SECONDS]} seconds passed, avg time #{get_avg(avg_diff).round(2)} msec, let's try sockets now\n\n"
    break
  end
end

# Socket to compare
require 'socket'

host = URI(@config[:URL_TO_PING].to_s).host
port = '443'
avg_diff = []

start_time = Time.now.to_i
loop do

  start = Time.now
  s = TCPSocket.open host, port
  s.puts "GET / HTTP/1.1\r\n"
  s.puts "\r\n"

  while line = s.gets
    puts line.chop
  end
  stop = Time.now

  msec_diff = time_diff(start, stop)
  avg_diff << msec_diff

  puts "#{@success} pinged #{host} with 'socket' package got response time #{msec_diff} msec"

  s.close
  if Time.now.to_i - start_time >= @config[:STOP_IN_SECONDS].to_i
    puts "\n\n#{@config[:STOP_IN_SECONDS]} seconds passed, avg time #{get_avg(avg_diff).round(2)} msec. Note 'sockets' didn't follow redirects.\n"
    exit 0
  end
end
