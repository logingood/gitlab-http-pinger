# Description

HTTP Pinger that allows to ping "https://gitlab.com" and other websites over a given time period. Can be useful to collect statistics.

# Configuration

Following environment variables control the script and they have default values as follows:
```
export STOP_IN_SECONDS=360
export URL_TO_PING='https://gitlab.com'
export FOLLOW_REDIRECT='true'
export LIMIT_REDIRECT=3
```

* STOP_IN_SECONDS - how long we want to ping the given url
* URL_TO_PING - http/https URL to fetch
* FOLLOW_REDIRECT - whether or not follow HTTP redirects
* LIMIT_REDIRECT - limits redirects to follow.

# Run

Tested on ruby 2.6.5, should work on most of version.

```
URL_TO_PING='https://gitlab.com' STOP_IN_SECONDS=10 ruby gtlabping.rb
```
